{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ODE tutorial\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Any, Callable, Iterable, Sequence, Union, cast\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pkg_resources\n",
    "from modelbase.ode import Model, Simulator\n",
    "from numpy.typing import NDArray\n",
    "from scipy.integrate import solve_ivp\n",
    "\n",
    "Vector = Union[Sequence[float], NDArray[np.float64]]\n",
    "Float = Union[float, np.float64]\n",
    "\n",
    "for pkg in (\"modelbase\", \"assimulo\"):\n",
    "    print(f\"{pkg} version: {pkg_resources.get_distribution(pkg).version}\")\n",
    "\n",
    "\n",
    "def time_points(\n",
    "    start: float, end: float, points: int\n",
    ") -> dict[str, tuple[float, float] | NDArray]:\n",
    "    t_eval = np.linspace(start, end, points)\n",
    "    t_span: tuple[float, float] = (t_eval[0], t_eval[-1])\n",
    "    return dict(t_span=t_span, t_eval=t_eval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numerical integration\n",
    "\n",
    "**Euler's method** is a numerical  approximation of the solution of a first-order initial value problem of the form $\\frac{dy}{dt} = f(t, y)$ with the inital condition $y(t_0) = y_0$.\n",
    "\n",
    "The strategy of Euler’s method is as follows: using a **tangent line approximation** of the unknown function, for $y_1$ close to $y_0$ we have\n",
    "  \n",
    "$$\\begin{align*}    \n",
    "y(t_1) &\\approx \\frac{dy(t_0)}{dt} (t_1-t_0) + y(t_0) \\\\\n",
    "   &= f(t_0, y_0) (t_1 - t_0) + y_0\n",
    "\\end{align*}$$\n",
    "\n",
    "This process is then repeated as often as needed, until the desired $t_{end}$ is reached.\n",
    "\n",
    "If we violate the assumption that $y_1$ close to $y_0$, we see that the tangent is not a good predictor of the next point anymore.\n",
    "\n",
    "![](assets/euler-tangent-step-size.png)\n",
    "\n",
    "It is thus important to always set the integration step size accordingly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Euler method\n",
    "\n",
    "\n",
    "**Step 1**\n",
    "\n",
    "Create a function to do a single euler step with the signature `step(t0: float, t1: float, y0: float, f: Callable[[float, float], float]) -> float`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def step(t0: float, t1: float, y0: float, f: Callable[[float, float], float]) -> float:\n",
    "    ...\n",
    "\n",
    "\n",
    "assert step(0, 1, 1, lambda t, y: y**2 + t) == 2\n",
    "assert step(0, 1, 2, lambda t, y: y**2 + t) == 6\n",
    "assert step(0, 1, 3, lambda t, y: y**2 + t) == 12"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 2**\n",
    "\n",
    "Create a function `euler` that takes an iterable of time points and iteratively calculates the euler approximation for the respective time step "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def euler(t: Vector, y0: float, f: Callable[[float, float], float]) -> list[float]:\n",
    "    ...\n",
    "\n",
    "\n",
    "assert euler([1, 2, 3], 1, lambda t, y: y**2 + t) == [1, 3, 14]\n",
    "assert euler(np.array([1, 2, 3]), 1, lambda t, y: y**2 + t) == [1, 3, 14]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 3**\n",
    "\n",
    "Using you `euler` function, numerically integrate the function\n",
    "\n",
    "$$ \\frac{dy}{dt} = - \\frac{y \\cdot t}{2}$$\n",
    "\n",
    "Which has the analytical solution\n",
    "\n",
    "$$ f(t, y_0) = y_0 \\cdot \\mathrm{e}^{-\\frac{t^2}{4}}$$\n",
    "\n",
    "Then create a plot comparing the two solutions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 4**\n",
    "\n",
    "- Investigate, how the fit of your numerical approximation varies, depending the selected step size\n",
    "- Create a 3x2 subplot of step sizes `[1, 1/2, 1/4, 1/8, 1/16, 1/32]` in which you plot both your numerical and your analytical solution "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More advanced methods\n",
    "\n",
    "While a smaller step size is more accurate, it also requires more computational power.  \n",
    "So you need to make a tradeoff between the time your integration can take and how well your function needs to be approximated.  \n",
    "How close enough a solution needs to be is a decision you need to make and depdends on your system.  \n",
    "A good enough starting point usually is to require both an absolute and relative tolerance of around $10^{-6}$.  \n",
    "\n",
    "Another observation is that most functions have regions of varying curvature.  \n",
    "More advanced methods take this into account and adjust the step size dynamically to the local curvature. \n",
    "\n",
    "Instead of implementing these ourself, we will take a look at two packages to compare their behaviour.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's  first take a look at the `solve_ivp` (ivp = initial value problem) method from the popular [scipy](https://scipy.org/) package.  \n",
    "\n",
    "The required inputs of this functions are \n",
    "\n",
    "- the python function `f(t, y)`\n",
    "- start and end point of the integration\n",
    "- initial values\n",
    "\n",
    "Optionally, you can also supply tolerances etc, for more information see the function docstring or [the documentation](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html?highlight=solve_ivp#scipy.integrate.solve_ivp)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: integration using scipy\n",
    "\n",
    "- Investigate, how the fit of the `solve_ivp` approximation varies, depending the selected tolerance\n",
    "- Create a 3x2 subplot in which you plot both your numerical and your analytical solution for tolerances between `1e-1` and `1e-6`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Optional**\n",
    "\n",
    "You can also use the assimulo package, which provides access to the efficient [sundials](https://computing.llnl.gov/projects/sundials) solver suite."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: integration using assimulo\n",
    "\n",
    "- Investigate, how the fit of the `assimulo` approximation varies, depending the selected tolerance\n",
    "- Create a 3x2 subplot in which you plot both your numerical and your analytical solution for tolerances between `1e-1` and `1e-6`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- How did the two integrators differ?  \n",
    "- Did you get the same results for the same tolerances?  \n",
    "\n",
    "Remember to always check your tools before you are using them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ODE model building\n",
    "\n",
    "## SIR model\n",
    "\n",
    "- Susceptible (S), Infected (I), Recovered (R)\n",
    "- Total population (N) = S + I + R\n",
    "\n",
    "$$\\begin{align*}\n",
    "    \\frac{dS}{dt} &= -\\beta \\frac{S \\cdot I}{N}  \\\\\n",
    "    \\frac{dI}{dt} &= \\beta \\frac{S \\cdot I}{N} - \\gamma \\cdot I \\\\\n",
    "    \\frac{dR}{dt} &= \\gamma \\cdot I \\\\\n",
    "\\end{align*}$$\n",
    "\n",
    "Let's start with a literal translation of that description."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sir_v1(t: float, y: Iterable[float], beta: float, gamma: float) -> Iterable[float]:\n",
    "    s, i, r = y\n",
    "    n = s + i + r\n",
    "\n",
    "    dsdt = -beta * s * i / n\n",
    "    didt = beta * s * i / n - gamma * i\n",
    "    drdt = gamma * i\n",
    "    return dsdt, didt, drdt\n",
    "\n",
    "\n",
    "sol = solve_ivp(sir_v1, **time_points(0, 10, 100), y0=[0.9, 0.1, 0], args=(2, 0.1))\n",
    "t = sol.t\n",
    "y = sol.y.T\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(10, 8))\n",
    "ax.plot(t, y)\n",
    "ax.set(xlabel=\"time / a.u.\", ylabel=\"y / a.u.\")\n",
    "ax.legend([\"S\", \"I\", \"R\"], loc=\"upper left\", bbox_to_anchor=(1.02, 1), borderaxespad=0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a lot of duplication in the code above (e.g. `beta * s * i / n` appears twice), let's improve on that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sir_v2(t: float, y: Iterable[float], beta: float, gamma: float) -> Iterable[float]:\n",
    "    s, i, r = y\n",
    "    n = s + i + r\n",
    "\n",
    "    infection = beta * s * i / n\n",
    "    recovery = gamma * i\n",
    "\n",
    "    dsdt = -infection\n",
    "    didt = infection - recovery\n",
    "    drdt = recovery\n",
    "    return dsdt, didt, drdt\n",
    "\n",
    "\n",
    "t_eval = np.linspace(0, 10, 100)\n",
    "t_span = (t_eval[0], t_eval[-1])\n",
    "\n",
    "sol = solve_ivp(sir_v1, t_span=t_span, t_eval=t_eval, y0=[0.9, 0.1, 0], args=(2, 0.1))\n",
    "t = sol.t\n",
    "y = sol.y.T\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(10, 8))\n",
    "ax.plot(t, y)\n",
    "ax.set(xlabel=\"time / a.u.\", ylabel=\"y / a.u.\")\n",
    "ax.legend([\"S\", \"I\", \"R\"], loc=\"upper left\", bbox_to_anchor=(1.02, 1), borderaxespad=0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All cool and stuff, but very it is very easy to introduce subtle errors\n",
    "\n",
    "- order of unpacked values `s, i , r = y` and initial arguments `y0=[0.9, 0.1, 0]` might not be the same\n",
    "- order of parameters `beta, gamma` might not be the same as `args=(2, 0.1)`\n",
    "\n",
    "Inconveniences\n",
    "\n",
    "- cannot get readouts for the rates of infection and recovery at each time in point directly\n",
    "- with more parameters, model function will get vary large\n",
    "- it's hard to programmatically extend / change the model\n",
    "\n",
    "While this works well for small models, changing the model or creating model-variants requires you to re-write that function, probably introducing errors along the way.  \n",
    "What you want is to be able to have a *modular* interface to building your models.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# modelbase introduction\n",
    "\n",
    "*modelbase* supplies you with the `Model` object, which you can use to iteratively build your models.  \n",
    "Since *modelbase* is mostly used for work on biochemical networks, the terminology is geared towards that.  \n",
    "Whenever confusing, I will add a synonym that might be more applicable for the current model.  \n",
    "\n",
    "Let's begin by creating the model, and adding compounds (variables) and parameters (constants).  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = Model()\n",
    "m.add_compounds([\"S\", \"I\", \"R\"])\n",
    "m.add_parameters(\n",
    "    {\n",
    "        \"beta\": 2,  # new infections caused by one infection\n",
    "        \"gamma\": 0.1,  # turnover rate of infected (death or recovery)\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algebraic modules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will add an **algebraic module** to calculate the total population at each time point.  \n",
    "Algebraic modules are a way of expressing variables that change over time, but are not returned by the model function.  \n",
    "\n",
    "For this we first add a normal Python function for doing the calculation.  \n",
    "Note that when possible these should be general and re-usable whenever possible, to make your model clear to people reading it.  \n",
    "\n",
    "So the function below could also be called `sum` or `total`.  \n",
    "You can decide what makes most sense for your target audience - naming is communicating!  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def total_population(S: float, I: float, R: float) -> float:\n",
    "    return S + I + R"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Then we add an algebraic module using `.add_algebraic_module_from_args`.\n",
    "\n",
    "The following arguments are important for now:\n",
    "\n",
    "- `module_name`: name of the module. Has to be **unique**\n",
    "- `function`: The Python function you defined above\n",
    "- `derived_compounds`: list of derived compounds\n",
    "- `args`: A list of all arguments that will be passed to the Python function. The order is the order in which the function will receive the arguments!\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.add_algebraic_module_from_args(\n",
    "    module_name=\"N\",\n",
    "    function=total_population,\n",
    "    derived_compounds=[\"N\"],\n",
    "    args=[\"S\", \"I\", \"R\"],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reactions\n",
    "\n",
    "To add **reactions** (transitions), just like with algebraic modules we first define the Python functions.  \n",
    "Like with algebraic modules you should aim at using function names that you can re-use.  \n",
    "However, in this example it is more clear to just name them directly - there are exceptions to all rules :)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def infection(S: float, I: float, N: float, beta: float) -> float:\n",
    "    return beta * S * I / N\n",
    "\n",
    "\n",
    "def turnover(I: float, gamma: float) -> float:\n",
    "    return gamma * I"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will add the reactions to modelbase using the method `.add_reaction_from_args`.\n",
    "\n",
    "The following arguments are important for now:\n",
    "\n",
    "- `rate_name`: The name you want to give your rate. Has to be **unique** \n",
    "- `function`: The Python function you defined above\n",
    "- `stoichiometry`: A dictionary defining by how much compounds will be *consumed* (negative value) or *produced* (positive value) by the reaction\n",
    "- `args`: A list of all arguments that will be passed to the python function. The order is the order in which the function will receive the arguments!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.add_reaction_from_args(\n",
    "    rate_name=\"infection\",\n",
    "    function=infection,\n",
    "    stoichiometry={\"S\": -1, \"I\": 1},\n",
    "    args=[\"S\", \"I\", \"N\", \"beta\"],\n",
    ")\n",
    "m.add_reaction_from_args(\n",
    "    rate_name=\"turnover\",\n",
    "    function=turnover,\n",
    "    stoichiometry={\"I\": -1, \"R\": 1},\n",
    "    args=[\"I\", \"gamma\"],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulating\n",
    "\n",
    "Now we are already ready to simulate the model!  \n",
    "To do that, create a `Simulator` object by passing the model into it and then initialise the simulator with a dictionary containing the **initial conditions** of the model.\n",
    "Then you can simulate the model until time point `t_end = 10` and plot the result.  \n",
    "\n",
    "There are a lot more plotting functions, especially for larger models.  \n",
    "Since they are all named `plot_*`, the easiest way you can explore them is to write `s.plot_` and then use the `<TAB>` key for autocompletion.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = Simulator(m)\n",
    "s.initialise({\"S\": 900, \"I\": 10, \"R\": 0})\n",
    "s.simulate(10)\n",
    "fig, ax = s.plot(title=\"SIR model\", xlabel=\"time / a.u.\", ylabel=\"concentration / a.u.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we used the derived compound `N` for the total population in our reactions, but we don't see it in the plot.  \n",
    "This is because derived compounds are not returned to the integrator, and thus not part of the results.  \n",
    "However, modelbase offers an easy function to plot derived compounds as well, using `plot_all`.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = s.plot_all(\n",
    "    title=\"SIR model\", xlabel=\"time / a.u.\", ylabel=\"concentration / a.u.\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can **continue** a simulation by running `.simulate` again with a *larger* end time point.  \n",
    "This will not re-run previous time steps.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.simulate(100)\n",
    "fig, ax = s.plot(xlabel=\"time / a.u.\", ylabel=\"Concentration / a.u.\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can calculate the steady state, where $Nv = 0$ using `.simulate_to_steady_state`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_, y_ss = s.simulate_to_steady_state()\n",
    "print(y_ss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Very often you want to systematically investigate the **steady-state concentrations** depending on different values of a parameter.  \n",
    "For this you can use the `Simulator.parameter_scan` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_scan = s.parameter_scan(parameter_name=\"k_in\", parameter_values=np.linspace(0, 10, 11))\n",
    "print(y_scan)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are also interested in the **steady-state fluxes**, you can use the `Simulator.parameter_scan_with_fluxes` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_scan, v_scan = s.parameter_scan_with_fluxes(\n",
    "    parameter_name=\"k_in\", parameter_values=np.linspace(0, 10, 11)\n",
    ")\n",
    "print(v_scan)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introspection & bug fixing\n",
    "\n",
    "Not all the times model creation goes as smoothly as in the example.  \n",
    "To allow you to quickly find an error, modelbase contains introspection methods, to quickly find out where things went wrong.  \n",
    "We can check the constructed system, by taking a look at the stoichiometric matrix, the fluxes and the right hand side.\n",
    "\n",
    "Let's start with the stoichiometric matrix, which modelbase returns as a `dataframe`.  \n",
    "Here you can see which reaction affects which compound by how much.  \n",
    "If a number here is unexpected, you know that you need to change the respective `stoichiometry` dictionary.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.get_stoichiometric_df()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next step is to check the fluxes of the system.  \n",
    "This is especially useful to see whether the direction of all reactions is as expected or to check if a reaction is overly large or small"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.get_fluxes_df({\"S\": 0.9, \"I\": 0.1, \"R\": 0})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You *can* also check the right hand side of the differential equation.  \n",
    "Since that is just the stoichiometric matrix multiplied by the fluxes this doesn't give any new information, but it is an easy way to check if all reactions are going in the correct direction.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.get_right_hand_side({\"S\": 0.9, \"I\": 0.1, \"R\": 0})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some bugs only appear after a certain amount of time.  \n",
    "The simulator object allows checking the concentrations over time (results) and fluxes over time.  \n",
    "\n",
    "If you need to access the concentrations over time for something other than plotting, you can do that using `.get_results_df()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.get_results_df().head(5)  # type: ignore"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A just like with the model you can get the fluxes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.get_fluxes_df().head(5)  # type: ignore"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model interaction\n",
    "\n",
    "Creating a model is fun, but interactively working with it is even better.  \n",
    "You can at any point add, remove or update any part of the model.  \n",
    "If you want to re-use the same simulator, run `.clear_results()` to reset the simulator, or `.initialise` if the number of compounds has changed.  \n",
    "In most cases however, we advise to simply create a new `Simulator` object.  \n",
    "\n",
    "\n",
    "### Example: extending model with population dynamics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def proportional(x: float, y: float) -> float:\n",
    "    return x * y\n",
    "\n",
    "\n",
    "def births(n: float, max_birth_rate: float, capacity: float) -> float:\n",
    "    return max_birth_rate * (capacity - n) / capacity * n\n",
    "\n",
    "\n",
    "m.add_parameters(\n",
    "    {\n",
    "        \"capacity\": 1000,\n",
    "        \"max_birth_rate\": 1,\n",
    "        \"death_rate_normal\": 0.1,\n",
    "        \"death_rate_infected\": 1,\n",
    "    }\n",
    ")\n",
    "\n",
    "m.add_reaction_from_args(\n",
    "    rate_name=\"births_from_s\",\n",
    "    function=births,\n",
    "    stoichiometry={\"S\": 1},\n",
    "    args=[\"N\", \"max_birth_rate\", \"capacity\"],\n",
    ")\n",
    "m.add_reaction_from_args(\n",
    "    rate_name=\"death_s\",\n",
    "    function=proportional,\n",
    "    stoichiometry={\"S\": -1},\n",
    "    args=[\"S\", \"death_rate_normal\"],\n",
    ")\n",
    "m.add_reaction_from_args(\n",
    "    rate_name=\"death_i\",\n",
    "    function=proportional,\n",
    "    stoichiometry={\"I\": -1},\n",
    "    args=[\"I\", \"death_rate_infected\"],\n",
    ")\n",
    "m.add_reaction_from_args(\n",
    "    rate_name=\"death_r\",\n",
    "    function=proportional,\n",
    "    stoichiometry={\"R\": -1},\n",
    "    args=[\"R\", \"death_rate_normal\"],\n",
    ")\n",
    "\n",
    "s = Simulator(m)\n",
    "s.initialise({\"S\": 900, \"I\": 10, \"R\": 0})\n",
    "s.simulate(100)\n",
    "s.update_parameter(\"beta\", 2)\n",
    "fig, ax = s.plot_all()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Derived parameters\n",
    "\n",
    "Quite often we have parameters that depend on other parameters.  \n",
    "That means they don't change during the simulation, but they can change during runs if you change another parameter.  \n",
    "You can add such a derived parameter using `add_derived_parameter`, which takes the following arguments\n",
    "\n",
    "- `parameter_name`: name of the derived parameter\n",
    "- `function`: Python function\n",
    "- `parameters`: Iterable of inputs for the Python function.\n",
    "\n",
    "`modelbase` will then do the housekeeping for you to automatically update the derived parameter if you change one of the parameters it depends on.  \n",
    "\n",
    "```python\n",
    "m = Model()\n",
    "m.add_parameter(\"R\", 8.314)                               # gas constant\n",
    "m.add_parameter(\"T\", 298)                                 # 25 °C\n",
    "m.add_derived_parameter(\"RT\", proportional, [\"R\", \"T\"])\n",
    "print(m.parameters)                                       # {'R': 8.314, 'T': 298, 'RT': 2477.572}\n",
    "\n",
    "# Note how RT updates as well\n",
    "m.update_parameter(\"T\", 303)                              # 303 °C\n",
    "print(m.parameters)                                       # {'R': 8.314, 'T': 303, 'RT': 2519.142}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: FILL ME"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reducing state\n",
    "\n",
    "When you are exploring your model there is a high chance that you will change something and forget about it.  \n",
    "Programming in such a *stateful* way is prone to errors, which is why there are two things you can do to minimise the amount of state.  \n",
    "First, you can use a *context manager* to only apply changes to a model in an indented block.\n",
    "\n",
    "```python\n",
    "m = Model()\n",
    "print(m.parameters)             # {}\n",
    "\n",
    "with m as temp:\n",
    "    temp.add_parameter(\"x\", 1)  \n",
    "    print(temp.parameters)      # {'x': 1}\n",
    "\n",
    "print(m.parameters)             # {}\n",
    "```\n",
    "\n",
    "Secondly, you can modularise building your models by putting them into isolated functions.  \n",
    "Note that below `create_model_v2` calls `create_model_v1`, instead of repeating all the steps in `create_model_v1`.  \n",
    "This makes it very clear which parts of your model are actually changing and avoids errors where you only applied a change in *one* of potentially many models.    \n",
    "\n",
    "```python\n",
    "def create_model_v1() -> Model:\n",
    "    m = Model()\n",
    "    m.add_compounds([\"S\", \"P\"])\n",
    "    m.add_parameters({\"k_in\": 1, \"k_1\": 1, \"k_out\": 1})\n",
    "    m.add_reaction_from_args(\"v0\", constant, {\"S\": 1}, [\"k_in\"])\n",
    "    m.add_reaction_from_args(\"v1\", proportional, {\"S\": -1, \"P\": 1}, [\"k_1\", \"S\"])\n",
    "    m.add_reaction_from_args(\"v2\", proportional, {\"P\": -1}, [\"k_out\", \"P\"])\n",
    "    return m\n",
    "\n",
    "\n",
    "def create_model_v2() -> Model:\n",
    "    m = create_model_v1()\n",
    "    m.add_parameter(\"k_1_rev\", 0.5)\n",
    "    m.add_reaction_from_args(\"v1_rev\", proportional, {\"P\": -1, \"S\": 1}, [\"k_1_rev\", \"P\"])\n",
    "    return m\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: FILL ME"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bonus: Label models\n",
    "\n",
    "<!-- ![label model](assets/tpi.png) -->\n",
    "<img src=\"assets/tpi.png\" style=\"max-width: 400px\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = {\n",
    "    \"kf_TPI\": 1.0,\n",
    "    \"Keq_TPI\": 21.0,\n",
    "    \"kf_Ald\": 2000.0,\n",
    "    \"Keq_Ald\": 7000.0,\n",
    "}\n",
    "\n",
    "GAP0 = 2.5e-5\n",
    "DHAP0 = GAP0 * p[\"Keq_TPI\"]\n",
    "FBP0 = GAP0 * DHAP0 * p[\"Keq_Ald\"]\n",
    "\n",
    "base_y0 = {\"GAP\": GAP0, \"DHAP\": DHAP0, \"FBP\": FBP0}\n",
    "\n",
    "p[\"kr_TPI\"] = p[\"kf_TPI\"] / p[\"Keq_TPI\"]\n",
    "p[\"kr_Ald\"] = p[\"kf_Ald\"] / p[\"Keq_Ald\"]\n",
    "\n",
    "\n",
    "def mass_action_1(kf, s):\n",
    "    return kf * s\n",
    "\n",
    "\n",
    "def mass_action_2(kf, s1, s2):\n",
    "    return kf * s1 * s2\n",
    "\n",
    "\n",
    "m = Model()\n",
    "m.add_compounds([\"GAP\", \"DHAP\", \"FBP\"])\n",
    "m.add_parameters(p)\n",
    "m.add_reaction_from_args(\"TPIf\", mass_action_1, {\"GAP\": -1, \"DHAP\": 1}, [\"kf_TPI\", \"GAP\"])\n",
    "m.add_reaction_from_args(\n",
    "    \"TPIr\", mass_action_1, {\"DHAP\": -1, \"GAP\": 1}, [\"kr_TPI\", \"DHAP\"]\n",
    ")\n",
    "m.add_reaction_from_args(\n",
    "    \"ALDf\", mass_action_2, {\"DHAP\": -1, \"GAP\": -1, \"FBP\": 1}, [\"kf_Ald\", \"DHAP\", \"GAP\"]\n",
    ")\n",
    "m.add_reaction_from_args(\n",
    "    \"ALDr\",\n",
    "    mass_action_1,\n",
    "    {\n",
    "        \"FBP\": -1,\n",
    "        \"DHAP\": 1,\n",
    "        \"GAP\": 1,\n",
    "    },\n",
    "    [\"kr_Ald\", \"FBP\"],\n",
    ")\n",
    "\n",
    "# Simulate\n",
    "s = Simulator(m)\n",
    "s.initialise(base_y0)\n",
    "t, y = s.simulate(20)\n",
    "s.plot()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lm = m.to_labelmodel(\n",
    "    labelcompounds={\"GAP\": 3, \"DHAP\": 3, \"FBP\": 6},\n",
    "    labelmaps={\n",
    "        \"TPIf\": [2, 1, 0],\n",
    "        \"TPIr\": [2, 1, 0],\n",
    "        \"ALDf\": [0, 1, 2, 3, 4, 5],\n",
    "        \"ALDr\": [0, 1, 2, 3, 4, 5],\n",
    "    },\n",
    ")\n",
    "\n",
    "\n",
    "# Simulate\n",
    "y0 = lm.generate_y0(base_y0, label_positions={\"GAP\": 0})\n",
    "ls = Simulator(lm)\n",
    "ls.initialise(y0)\n",
    "t, y = ls.simulate(20)\n",
    "fig, ax = ls.plot_label_distribution_grid(\n",
    "    [\"GAP\", \"DHAP\", \"FBP\"],\n",
    "    ncols=3,\n",
    "    sharey=False,\n",
    "    relative=True,\n",
    "    xlabels=\"Time [au]\",\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "efa8b17b9e33237cbcac2860a8fba36b27177a72c7cfe842f3a84d2139868231"
  },
  "kernelspec": {
   "display_name": "Python 3.10.4 ('py310')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "384px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
